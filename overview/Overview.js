define([
  'jquery',
  'dojo/_base/lang',
  'dojo/_base/declare',
  'dojo/promise/all',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  'di18n/localize',
  'entryscape-commons/overview/components/Overview',
  'entryscape-commons/view/ViewMixin',
  'entryscape-commons/defaults',
  'entryscape-commons/util/dateUtil',
  'mithril',
  'i18n!nls/esteOverview',
], (jquery, lang, declare, all, domAttr, domStyle, _WidgetBase, _TemplatedMixin,
    _WidgetsInTemplateMixin, NLSMixin, localize, Overview, ViewMixin, defaults, dateUtil) =>
  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit, ViewMixin], {
    templateString: '<div class="termsOverview"></div>',
    nlsBundles: ['esteOverview'],
    localeChange() {
      this.show();
    },
    show() {
      this.data = {};
      let concepts;
      let scheme;
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      const esu = defaults.get('entrystoreutil');
      const rdfutils = defaults.get('rdfutils');
      const ns = defaults.get('namespaces');
      const context = defaults.get('context');
      const spa = defaults.get('siteManager');
      const currentOrUpcomingParams = spa.getUpcomingOrCurrentParams();
      const sl = es.newSolrQuery().rdfType(ns.expand('skos:Concept')).context(context)
        .sort('modified+desc').limit(1).list();
      const collectionsQuery = es.newSolrQuery().rdfType(ns.expand('skos:Collection'))
        .context(context).limit(0).list();
      const collectionsQueryPromise = collectionsQuery.getEntries(0);

      const conceptsPromise = sl.getEntries(0).then((entries) => {
        concepts = entries;
      });

      const schemePromise = esu.getEntryByType('skos:ConceptScheme', context).then((skosEntry) => {
        scheme = skosEntry;
      });

      all([this.localeReady, conceptsPromise, schemePromise, collectionsQueryPromise]).then(() => {
        const modificationDate = concepts.length > 0 ?
          concepts[0].getEntryInfo().getModificationDate() :
          scheme.getEntryInfo().getModificationDate();
        const creationDate = scheme.getEntryInfo().getCreationDate();
        const modificationDateFormats = dateUtil.getMultipleDateFormats(modificationDate);
        const creationDateFormats = dateUtil.getMultipleDateFormats(creationDate);

        const isNLSBundleReady = this.nlsBundles[0] in this.NLSBundles;
        let b;
        if (isNLSBundleReady) {
          b = this.NLSBundles[this.nlsBundles[0]];
        }

        // basic info
        this.data.description = rdfutils.getDescription(scheme);
        this.data.title = rdfutils.getLabel(scheme);

        // box list
        this.data.bList = [];
        this.data.bList.push(
          {
            key: 'terms',
            label: isNLSBundleReady ? localize(b, 'termsLabel') : 'termsLabel',
            value: sl.getSize(),
            link: spa.getViewPath('terminology__hierarchy', currentOrUpcomingParams),
          },
          {
            key: 'collections',
            label: isNLSBundleReady ? localize(b, 'collectionsLabel') : 'collectionsLabel',
            value: collectionsQuery.getSize(),
            link: spa.getViewPath('terminology__collections', currentOrUpcomingParams),
          });

        // stats list
        this.data.sList = [];
        this.data.sList.push(
          {
            key: 'update',
            label: isNLSBundleReady ? localize(b, 'lastUpdatedLabel') : 'lastUpdatedLabel',
            value: modificationDateFormats.short,
          },
          {
            key: 'create',
            label: isNLSBundleReady ? localize(b, 'createdLabel') : 'createdLabel',
            value: creationDateFormats.short,
          });

        m.render(jquery('.termsOverview')[0], m(Overview, { data: this.data }));
      });
    },
  }));
