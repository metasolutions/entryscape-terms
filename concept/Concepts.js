define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Deferred',
  'dojo/aspect',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dojo/keys',
  'config',
  'entryscape-terms/utils',
  'entryscape-commons/rdforms/EntryChooser',
  'entryscape-commons/placeholder/Placeholder',
  'entryscape-commons/view/ViewMixin',
  'rdforms/view/renderingContext',
  'rdforms/view/Editor' /* NMD:Ignore*/, // In template
  'rdforms/view/bootstrap/LevelEditor' /* NMD:Ignore*/, // In template
  'entryscape-commons/tree/Tree' /* NMD:Ignore*/, // In template
  'entryscape-commons/defaults',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin'/* NMD:Ignore*/,
  'di18n/NLSMixin',
  'di18n/localize',
  'jquery',
  'dojo/text!./ConceptsTemplate.html',
  'entryscape-commons/tree/skos/repair',
  'entryscape-commons/tree/skos/util',
  'i18n!nls/esteConcept',
], (declare, lang, Deferred, aspect, domAttr, domStyle, domClass, keys, config, utils, EntryChooser,
    Placeholder, ViewMixin, renderingContext, Editor, LevelEditor, Tree, defaults, _WidgetBase,
    _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin, localize, jquery, template,
    skosRepair, skosUtil) => {
  const isNodeInLi = (leaf, parent) => {
    if (leaf === parent) {
      return false;
    } else if (leaf.nodeName === 'LI') {
      return true;
    }
    return isNodeInLi(leaf.parentNode, parent);
  };

  EntryChooser.registerDefaults();

  const ConceptPlaceholder = declare([Placeholder], {
    missingImageClass: 'cube',
    getText() {
      return this.concepts.NLSBundle0.emptyTreeWarning;
    },
  });

  return declare(
    [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit, ViewMixin], {
      templateString: template,
      nlsBundles: ['esteConcept'],

      postCreate() {
        this._conceptTree.disallowedSiblingMove = () => {
          this.localeReady.then(() => {
            const message = localize(this.NLSBundle0, 'cannotReorderTerm');
            defaults.get('dialogs').acknowledge(message);
          });
        };
        this.inherited(arguments);
      },
      show(viewParams) {
        this.inherited('show', arguments);
        this.currentParams = viewParams;
        delete this.currentSelectedEntry;
        const context = defaults.get('context');
        this.unBindEvents();
        this.showEditorPlaceholder();

        defaults.get('entrystoreutil').getEntryByType('skos:ConceptScheme', context)
          .then((csEntry) => {
            this.conceptScheme = csEntry;
            this._conceptTree.showEntry(csEntry);

            // every time model consistency is updated check if you need to show an error
            aspect.after(this._conceptTree.getTreeModel(), 'setModelConsistency', lang.hitch(this, this._toggleTreeError));

            const label = defaults.get('rdfutils').getLabel(csEntry);
            domAttr.set(this.__selectedTerm, 'innerHTML', label);

            this._conceptTree.getTreeModel().isMoveAllowed =
              (child, from, to) => utils.isUnModified([child, from, to]).then(null, () => {
                const b = this.NLSBundle0;
                return defaults.get('dialogs').confirm(b.concurrentConflictMessage, b.concurrentConflictOk).then(() => {
                  this.show(viewParams);
                  return false;
                });
              });
            this.bindEvents();
          });
      },

      bindEvents() {
        jquery(this._conceptTree.domNode).on('select_node.jstree', lang.hitch(this, (ev, obj) => {
          const entryPromise = this._conceptTree.getTreeModel().getEntry(obj.node);
          entryPromise.then(lang.hitch(this, this._treeClick));
          entryPromise.then(entry => defaults.set('entry', entry));
        }));
        const f = lang.hitch(this, this.clear);
        jquery(this._conceptTree.domNode).on('click', lang.hitch(this, function (ev) {
          ev.stopPropagation();
          if (!isNodeInLi(ev.target, ev.currentTarget)) {
            setTimeout(() => {
              this._askToProceedIfChanged().then(f);
            }, 1);
          }
        }));
      },
      unBindEvents() {
        jquery(this._conceptTree.domNode).off('select_node.jstree');
        jquery(this.sidebar).off();
      },
      clear() {
        if (this.currentSelectedEntry) {
          const node = this._conceptTree.getTreeModel().getNode(this.currentSelectedEntry);
          this._conceptTree.getTree().deselect_all(true);
          this._conceptTree.getTree().dehover_node(node);

          domStyle.set(this._editor.domNode, 'display', 'none');
          delete this.currentSelectedEntry;
          this._updateButtons();
          this.showEditorPlaceholder();
        }
        // this.__placeholder
      },
      /**
       * Clears the current editor panel and shows a placeholder.
       */
      showEditorPlaceholder() {
        domStyle.set(this.__placeholder, 'display', '');
        domStyle.set(this.__editorBlock, 'display', 'none');
        domClass.remove(this.__conceptEditorMain, 'active');
      },
      localeChange() {
        this._updateButtons();
        if (!this.placeholder) {
          this.placeholder = new ConceptPlaceholder({ concepts: this }, this.__placeholder);
          this.__placeholder = this.placeholder.domNode;
        }
        this.placeholder.render();
      },
      _checkKey(ev) {
        if (ev.keyCode === keys.ENTER) {
          this._newC();
        }
      },
      _newC() {
        const defLang = renderingContext.getDefaultLanguage();
        const termLabel = this._termLabel;
        const label = domAttr.get(termLabel, 'value');
        if (label === '' || label == null) {
          return;
        }
        const pe = defaults.get('createEntry')(null, 'skos:Concept');
        const ct = this._conceptTree;
        const tree = ct.getTree();
        const model = ct.getTreeModel();
        const selNode = ct.getSelectedNode();
        const uri = pe.getResourceURI();
        let graph = pe.getMetadata();
        let l;
        if (typeof defLang === 'string' && defLang !== '') {
          l = defLang;
        }

        skosUtil.addNewConceptStmts({
          md: graph,
          conceptRURI: uri,
          schemeRURI: this.conceptScheme.getResourceURI(),
          isRoot: selNode ? false : true,
          label,
          l,
        });

        model.createEntry(pe, selNode || model.getRootNode(), tree).then(() => {
          domAttr.set(termLabel, 'value', '');
        });
      },
      _treeClick(entry) {
        domClass.add(this.__conceptEditorMain, 'active');

        if (entry === this.currentSelectedEntry) {
          return;
        }
        const label = defaults.get('rdfutils').getLabel(entry);
        domAttr.set(this.__selectedTerm, 'innerHTML', label);
        this._askToProceedIfChanged().then(() => this._updateSelected(entry));
      },
      _askToProceedIfChanged() {
        if (this.currentSelectedEntry != null && this.currentMetadata.isChanged()) {
          return defaults.get('dialogs')
            .confirm(
              this.NLSBundle0.conceptChangedMessage,
              this.NLSBundle0.continueAbandonChanges,
              this.NLSBundle0.continueEditing)
            .then(null, () => {
              const tree = this._conceptTree.getTree();
              const model = this._conceptTree.getTreeModel();
              tree.deselect_all();
              tree.select_node(model.getNode(this.currentSelectedEntry));
              throw Error('Do not proceed.');
            });
        }
        const d = new Deferred();
        d.resolve();
        return d;
      },
      _updateSelected(entry) {
        if (entry == null) {
          return;
        }

        // Refresh concept to avoid concurrent editing as far as possible.
        entry.setRefreshNeeded();
        entry.refresh().then(() => {
          const itemstore = defaults.get('itemstore');
          this.currentSelectedEntry = entry;
          domStyle.set(this.__editorBlock, 'display', '');
          domStyle.set(this.__placeholder, 'display', 'none');
          this.currentMetadata = entry.getMetadata().clone();
          const md = this.currentMetadata;

          if (this._editedSignal) {
            this._editedSignal.remove();
          }
          this._editedSignal = aspect.after(md, 'onChange', () => {
            this._updateButton('save', 1);
            this._updateButton('revert', 1);
          });
          this._updateButtons();

          domStyle.set(this._editor.domNode, 'display', '');
          const templateId = config.terms.conceptTemplateId || 'skosmos:concept';
          this._editor.show(entry.getResourceURI(), md, itemstore.getItem(templateId));
        });
      },
      _saveC() {
        if (!this.currentSelectedEntry) {
          return;
        }
        this._updateButton('save', 2);
        this.currentSelectedEntry.setMetadata(this.currentMetadata);
        this.currentSelectedEntry.commitMetadata().then(() => {
          this._conceptTree.refresh(this.currentSelectedEntry, false);
          this.currentMetadata.setChanged(false);
          this._updateButtons();
        });
      },
      _deleteC() {
        if (!this.currentSelectedEntry) {
          return;
        }
        const entryRURI = this.currentSelectedEntry.getResourceURI();
        const hasChildrenOrRelatedConcepts =
          skosUtil.hasChildrenOrRelationsConcepts(this.currentSelectedEntry);

        if (hasChildrenOrRelatedConcepts) {
          defaults.get('dialogs').acknowledge(this.NLSBundle0.cannotRemoveTermTree);
        } else {
          const label = defaults.get('rdfutils').getLabel(this.currentSelectedEntry) || this.currentSelectedEntry.getId();
          const message = localize(this.NLSBundle0, 'confirmRemoveTerm', label);
          defaults.get('dialogs').confirm(message).then(() => {
            this._conceptTree.deleteNode().then(() => {
              // for each SKOS mappings delete those mappings
              const prom = skosUtil.getMappingRelations(entryRURI);
              prom.then((mappingRelations) => {
                mappingRelations.forEach((entries, mappedProperty) => {
                  entries.forEach((mappedEntry) => {
                    mappedEntry.getMetadata()
                      .findAndRemove(mappedEntry.getResourceURI(), mappedProperty, entryRURI);

                    mappedEntry.commitMetadata();
                    mappedEntry.setRefreshNeeded();
                    mappedEntry.refresh();
                  });
                });
              });

              this.clear();
            });
          });
        }
      },
      _revertC() {
        if (!this.currentSelectedEntry) {
          return;
        }
        this._updateSelected(this.currentSelectedEntry);
      },
      _updateButtons() {
        if (this.currentSelectedEntry) {
          const state = this.currentMetadata.isChanged() ? 1 : 0;
          const isRoot = this._conceptTree.getTreeModel().isRoot(this.currentSelectedEntry);
          this._updateButton('save', state);
          this._updateButton('revert', state);
          this._updateButton('delete', isRoot ? 0 : 1);
        } else {
          this._updateButton('save', 0);
          this._updateButton('revert', 0);
          this._updateButton('delete', 0);
        }
      },
      _updateButton(action, state) {
        const node = this[`_${action}Button`];
        if (state === 1) { // Ready state
          domAttr.remove(node, 'disabled');
        } else {
          domAttr.set(node, 'disabled', 'disabled');
        }
        if (state === 2) { // Processing state
          domAttr.set(node, 'innerHTML', this.NLSBundle0[`${action}ProcessingButton`]);
        } else {
          domAttr.set(node, 'innerHTML', this.NLSBundle0[`${action}Button`]);
        }
      },
      /**
       * Called after TreeModel.setModelConsistency
       *
       * @private
       */
      _toggleTreeError() {
        if (!this._conceptTree.getTreeModel().isModelConsistent) {
          this.localeReady.then(() => {
            const message = localize(this.NLSBundle0, 'treeInconsistentMessage');
            const prompt = localize(this.NLSBundle0, 'treeInconsistentPromptLabel');
            defaults.get('dialogs').acknowledge(message, prompt).then(() => {
              this._fixTree();
            });
          });
        }
      },
      _fixTree() {
        const context = defaults.get('context');

        defaults.get('dialogs').progress(
          skosRepair.fix(context).then(() => {
            // tree model is consistent
            this._conceptTree.getTreeModel().setModelConsistency(true, true);
            const message = localize(this.NLSBundle0, 'treeFixSuccessMessage');
            defaults.get('dialogs').acknowledge(message).then(() => {
              this.show(this.currentParams);
            });
          }, () => {
            // TODO any other action?
            const message = localize(this.NLSBundle0, 'treeFixErrorMessage');
            defaults.get('dialogs').acknowledge(message);
          }));
      },
    });
});
