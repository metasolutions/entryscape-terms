define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-attr',
  '../defaults',
  'config',
  'di18n/NLSMixin',
  'di18n/localize',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/list/common/ETBaseList',
  'entryscape-commons/list/common/CreateDialog',
  'entryscape-commons/list/common/RemoveDialog',
  'entryscape-commons/tree/skos/ConceptRow',
  'entryscape-commons/tree/skos/util',
  'entryscape-commons/tree/TreeModel',
  'i18n!nls/escoList',
  'i18n!nls/esteConcept',
], (declare, lang, domAttr, defaults, config, NLSMixin, localize, EntryRow,
    ETBaseList, CreateDialog, RemoveDialog, ConceptRow, skosUtil, TreeModel) => {
  let treeModel;
  const ns = defaults.get('namespaces');

  const CCreateDialog = declare(CreateDialog, {
    open() {
      defaults.get('setConceptCount')(this.list.getView().getSize());
      if (!defaults.get('withinConceptLimit')()) {
        return;
      }
      this.inherited(arguments);
    },
    doneAction(graph) {
      const context = defaults.get('context');
      defaults.get('entrystoreutil').getEntryByType('skos:ConceptScheme', context)
        .then((csEntry) => {
          const md = skosUtil.addNewConceptStmts({
            md: graph,
            conceptRURI: this._newEntry.getResourceURI(),
            schemeRURI: csEntry.getResourceURI(),
            isRoot: true, // created from list
          });
          this._newEntry.setMetadata(md).commit().then((newCEntry) => {
            skosUtil.addConceptToScheme(newCEntry).then(() => {
              this.list.addRowForEntry(newCEntry);
              defaults.get('incrementConceptCount')();
            });
          });
        });
    },
  });

  const CRemoveDialog = declare([RemoveDialog, NLSMixin], {
    nlsBundles: ['esteConcept'],
    constructor() {
      this.initNLS();
    },
    open(params) {
      this.params = params;
      this.cEntry = params.row.entry;
      this.inherited('open', params);
      const label = defaults.get('rdfutils').getLabel(this.cEntry);
      let message;
      if (skosUtil.hasChildrenOrRelationsConcepts(this.cEntry)) {
        message = localize(this.NLSBundles.esteConcept, 'cannotRemoveTermTree', label);
        defaults.get('dialogs').acknowledge(message);
      } else {
        message = localize(this.NLSBundles.esteConcept, 'confirmRemoveTerm', label);
        defaults.get('dialogs').confirm(message).then(() => {
          this.remove();
        });
      }
    },
    clearRow() {
      this.params.row.destroy();
      this.list.removeRow(this.params.row);
      this.list.getView().clearSelection();
    },
    remove() {
      const entryRURI = this.cEntry.getResourceURI();
      treeModel.deleteEntry(this.cEntry).then(() => {
        // make UI changes
        this.clearRow();

        defaults.get('decrementConceptCount')();

        // for each SKOS mappings delete those mappings
        const prom = skosUtil.getMappingRelations(entryRURI);
        prom.then((mappingRelations) => {
          mappingRelations.forEach((entries, mappedProperty) => {
            entries.forEach((mappedEntry) => {
              mappedEntry.getMetadata()
                .findAndRemove(mappedEntry.getResourceURI(), mappedProperty, entryRURI);

              mappedEntry.commitMetadata();
              mappedEntry.setRefreshNeeded();
              mappedEntry.refresh();
            });
          });
        });
      });
    },
  });
  const SkosConceptRow = declare([ConceptRow], {
    hideTerminologyInPath: true,
  });

  return declare([ETBaseList], {
    includeCreateButton: true,
    includeInfoButton: true,
    includeEditButton: true,
    includeRemoveButton: true,
    nlsBundles: ['escoList', 'esteConcept'],
    entryType: ns.expand('skos:Concept'),
    rootEntryType: ns.expand('skos:ConceptScheme'),
    entitytype: 'concept',
    nlsCreateEntryLabel: 'createTerm',
    nlsCreateEntryTitle: 'createTermPopoverTitle',
    nlsCreateEntryMessage: 'createTermPopoverMessage',
    searchVisibleFromStart: true,
    rowClickDialog: 'info',
    rowClass: SkosConceptRow,
    versionExcludeProperties: [
      'skos:narrower',
      'skos:broader',
      'skos:topConceptOf',
      'dcterms:partOf',
    ],
    rowActionNames: ['info', 'edit', 'versions', 'remove'],

    postCreate() {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      const context = defaults.get('context');

      // although this is a list view, the underlying model is still a tree
      // and we use the model for operations like delete
      es.newSolrQuery().rdfType(this.rootEntryType).context(context).limit(1)
        .getEntries(0)
        .then((entries) => {
          // this is hack, get the first (and hopefully only) concept sceheme in this context
          treeModel = new TreeModel(
            Object.assign(skosUtil.getSemanticRelations(), { rootEntry: entries[0] }));
        });

      this.inherited('postCreate', arguments);
      this.registerDialog('create', CCreateDialog);
      this.registerDialog('remove', CRemoveDialog);
    },
    getTemplate() {
      const templateId = config.terms.conceptTemplateId || '';
      return defaults.get('itemstore').getItem(templateId);
    },
    getTemplateLevel() {
      return 'recommended';
    },
    getSearchObject() {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      const context = defaults.get('context');
      return es.newSolrQuery().rdfType(this.entryType).context(context);
    },
  });
});
