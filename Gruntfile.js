module.exports = (grunt) => {
  grunt.task.loadTasks('node_modules/entryscape-js/tasks');
  grunt.config.merge({
    poeditor: {
      projectids: ['94873'],
    },
    nls: {
      langs: ['en', 'sv', 'de'],
      depRepositories: ['entryscape-commons', 'entryscape-admin'],
    },
    update: {
      libs: [
        'di18n',
        'spa',
        'rdfjson',
        'rdforms',
        'store',
        'entryscape-commons',
        'entryscape-admin',
      ],
    },
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-available-tasks');
};
