/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    "lastUpdatedLabel": "Updated",
    "createdLabel": "Created",
    "termsLabel": "Concepts",
    "collectionsLabel": "Collections",
  },
  sv: true,
  de: true
});
