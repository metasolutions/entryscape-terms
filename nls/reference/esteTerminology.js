/* eslint quotes: ["error", "double"] */
/* eslint no-template-curly-in-string: "off" */
define({
  root: {
    createTerminology: "Create terminology",
    createTerminologyButton: "Create",
    importLabel: "Import",
    createLabel: "Create",
  },
  sv: true,
  de: true,
});
