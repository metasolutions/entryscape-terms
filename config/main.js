define([
  'entryscape-commons/merge',
  'entryscape-terms/config/termsConfig',
], (merge, termsConfig) => merge({
  theme: {
    appName: 'EntryScape Terms',
    oneRowNavbar: true,
    localTheme: false,
  },
  locale: {
    fallback: 'en',
    supported: [
                { lang: 'de', flag: 'de', label: 'Deutsch', labelEn: 'German', shortDatePattern: 'dd. MMM' },
                { lang: 'en', flag: 'gb', label: 'English', labelEn: 'English', shortDatePattern: 'MMM dd' },
                { lang: 'sv', flag: 'se', label: 'Svenska', labelEn: 'Swedish', shortDatePattern: 'dd MMM' },
    ],
  },
  site: {
    siteClass: 'entryscape-commons/nav/Site',
    controlClass: 'entryscape-commons/nav/Layout',
    startView: 'termsstart',
  },
}, termsConfig, __entryscape_config));
