require.config({
  baseUrl: './libs', // Path relative to bootstrapping html file.
  // Paths relative baseUrl, only those that deviate from baseUrl/{modulename} are explicitly listed
  paths: {
    'entryscape-terms': '..',
    templates: 'rdforms-templates',
    nls: '../nls/merged',
    theme: '../theme',
    text: 'requirejs-text/text',
    i18n: 'di18n/i18n',
    fuelux: 'fuelux/js',
    bootstrap: 'bootstrap-amd/lib',
    select2: 'select2/src/js',
    jquery: 'jquery/src',
    sizzle: 'sizzle/dist/sizzle',
    'jquery.mousewheel': 'select2/src/js/jquery.mousewheel.shim',
    typeahead: 'typeahead.js/dist/typeahead.jquery',
    jstree: 'jstree/src/jstree',
    'jstree.dnd': 'jstree/src/jstree.dnd',
    'jstree.checkbox': 'jstree/src/jstree.checkbox',
    'jstree.wholerow': 'jstree/src/jstree.wholerow',
    requireLib: 'requirejs/require',
    md5: 'md5/js/md5.min',
  },
  packages: [ // Config defined using packages to allow for main.js when requiring just config.
    {
      name: 'config',
      location: '../config',
      main: 'main',
    },
    {
      name: 'moment',
      main: 'moment',
    },
    {
      name: 'mithriljs',
      main: 'mithril',
    },
  ],
  shim: {
    typeahead: { deps: ['jquery'] },
  },
  map: {
    '*': {
      mithril: 'entryscape-commons/shim/mithril',
      jquery: 'jquery/jquery',  // In general, use the main module (for all unqualified jquery dependencies).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
      has: 'dojo/has', // Use dojos has module since it is more clever.
      'dojo/text': 'text', // Use require.js text module
      // Make sure i18n, dojo/i18n and di18n/i18n are all treated as a SINGLE module named i18n.
      // (We have mapped i18n to be the module provided in di18n/i18n, see paths above.)
      'dojo/i18n': 'i18n',
      'di18n/i18n': 'i18n',
      'dojo/hccss': 'dojo/has',
    },
    jquery: {
      jquery: 'jquery', // Reset (override general mapping) to normal path (jquerys has dependencies to specific modules).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
      'external/sizzle/dist/sizzle': 'sizzle',
    },
    bootstrap: {
      jquery: 'jquery', // Reset (override general mapping) to normal path (bootstraps has dependencies to specific dependencies).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
    },
    'store/rest': {
      'dojo/request': 'dojo/request/xhr', // Force using xhr since we know we are in the browser
      'dojo/request/iframe': 'dojo/request/iframe', // Override above line for iframe path.
    },
    'rdforms/template/bundleLoader': {
      'dojo/request': 'dojo/request/xhr',  // Force using xhr since we know we are in the browser
    },
  },
  deps: [
    'entryscape-commons/commonDeps',
    'entryscape-commons/nav/Cards',
    'entryscape-commons/gce/Cards',
    'entryscape-terms/scheme/List',
    'entryscape-terms/concept/Concepts',
    'entryscape-terms/concept/List',
    'entryscape-terms/collection/List',
    'entryscape-terms/overview/Overview',
    'entryscape-admin/contexts/List',
    'entryscape-admin/groups/List',
    'entryscape-admin/users/List',
    'entryscape-commons/rdforms/SkosChooser',
    'entryscape-commons/rdforms/EntryChooser',
    'templates/skos/skos',
    'templates/dcterms/dcterms',
    'templates/foaf/foaf',
    'templates/vcard/vcard',
    'templates/entryscape/esc',
    'select2/select2/i18n/sv', // Explicit load of swedish language for select2 (no require-nls support)
  ],
});
