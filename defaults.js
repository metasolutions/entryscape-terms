define([
  'entryscape-commons/defaults',
  'config',
  'entryscape-commons/rdforms/linkBehaviour',
], (defaults, config) => {
  const ns = defaults.get('namespaces');
  ns.add('skos', 'http://www.w3.org/2004/02/skos/core#');
  ns.add('esterms', 'http://entryscape.com/terms/');

  const cid2count = {};
  defaults.set('setConceptCount', (count) => {
    const cid = defaults.get('context').getId();
    cid2count[cid] = count;
  });
  defaults.set('incrementConceptCount', () => {
    const cid = defaults.get('context').getId();
    cid2count[cid] = typeof cid2count[cid] === 'undefined' ? 1 : cid2count[cid] + 1;
  });
  defaults.set('decrementConceptCount', () => {
    const cid = defaults.get('context').getId();
    cid2count[cid] = typeof cid2count[cid] === 'undefined' ? 0 : cid2count[cid] - 1;
  });
  defaults.set('getConceptCount', () => {
    const cid = defaults.get('context').getId();
    return cid2count[cid];
  });
  defaults.set('loadConceptCountIfNeeded', () => {
    const cid = defaults.get('context').getId();
    if (typeof cid2count[cid] === 'undefined') {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');

      const sl = es.newSolrQuery().rdfType('skos:Concept').context(defaults.get('context'))
        .sort('modified+desc').limit(0).list();
      sl.getEntries(0).then(() => {
        cid2count[cid] = sl.getSize();
      });
    }
  });
  defaults.set('withinConceptLimit', () => {
    const count = defaults.get('getConceptCount')();
    if (!defaults.get('hasAdminRights') && config.terms
      && parseInt(config.terms.conceptLimit, 10) === config.terms.conceptLimit) {
      let exception = false;
      const premiumGroupId = config.entrystore.premiumGroupId;
      if (premiumGroupId) {
        const es = defaults.get('entrystore');
        const groups = defaults.get('userEntry').getParentGroups();
        exception = groups.some(groupEntryURI => es.getEntryId(groupEntryURI) === premiumGroupId);
      }
      const premiumContextLevel = defaults.get('context')
        .getEntry(true)
        .getEntryInfo()
        .getGraph()
        .findFirstValue(null, 'store:premium');

      if (premiumContextLevel) {
        exception = true;
      }
      if (!exception && count >= parseInt(config.terms.conceptLimit, 10)) {
        defaults.get('dialogs').restriction(config.terms.conceptLimitDialog);
        return false;
      }
    }
    return true;
  });

  return defaults;
});
