define([
  'dojo/_base/declare',
  'config',
  'entryscape-commons/defaults',
  'entryscape-commons/gce/List',
  'entryscape-commons/gce/GCERow',
  'entryscape-admin/groups/MemberDialog',
  './CreateTerminologyDialog',
  'entryscape-commons/export/Export',
  'di18n/localize',
  'i18n!nls/escoList',
  'i18n!nls/esteTerminologyexport',
  'i18n!nls/esteScheme',
], (declare, config, defaults, List, GCERow, MemberDialog, CreateTerminologyDialog, Export,
    localize) => {
  const ns = defaults.get('namespaces');

  const ExportDialog = declare([Export], {
    nlsBundles: ['esteTerminologyexport'],
    nlsHeaderTitle: 'exportHeaderLabel',
    title: 'temporary', // to avoid exception
    profile: 'conceptscheme',
    open(params) {
      const name = defaults.get('rdfutils').getLabel(params.row.entry);
      this.title = localize(this.NLSBundle0, 'exportHeaderLabel', { name });
      this.localeChange();
      this.inherited(arguments);
    },
  });

  const TLMemberDialog = declare([MemberDialog.ListDialog], {
    open() {
      if (!defaults.get('hasAdminRights')
        && config.terms && config.terms.disallowTermCollaborationDialog) {
        defaults.get('dialogs').restriction(config.terms.disallowTermCollaborationDialog);
      } else {
        this.inherited(arguments);
      }
    },
  });

  const Row = declare([GCERow], {
    allowToggle() {
      if (config.catalog && config.terms.disallowSchemePublishingDialog != null
        && !defaults.get('hasAdminRights')) {
        defaults.get('dialogs').restriction(config.terms.disallowSchemePublishingDialog);
        return false;
      }

      return true;
    },
  });

  return declare([List], {
    includeCreateButton: true,
    includeInfoButton: false,
    includeEditButton: true,
    includeRemoveButton: true,
    includeExpandButton: false,
    nlsBundles: ['escoList', 'esteScheme'],
    rowClass: Row,

    nlsGCEPublicTitle: 'publicSchemeTitle',
    nlsGCEProtectedTitle: 'privateSchemeTitle',
    nlsGCESharingNoAccess: 'schemeSharingNoAccess',
    nlsGCEConfirmRemoveRow: 'confirmRemoveScheme',
    nlsGroupSharingProblem: 'schemeSharingProblem',
    rowClickView: 'termsoptions',
    entryType: ns.expand('skos:ConceptScheme'),
    contextType: 'esterms:TerminologyContext',
    versionExcludeProperties: ['skos:hasTopConcept'],
    rowActionNames: ['edit', 'versions', 'export', 'members', 'remove'],

    postCreate() {
      this.registerDialog('members', TLMemberDialog);

      this.registerRowButton({
        first: true,
        name: 'members',
        button: 'default',
        icon: 'users',
        iconType: 'fa',
        nlsKey: 'schemeMemberTitle',
      });
      this.registerDialog('export', ExportDialog);
      this.registerRowAction({
        first: true,
        name: 'export',
        button: 'default',
        icon: 'arrow-circle-o-down',
        iconType: 'fa',
        nlsKey: 'collectionExportTitle',
      });
      this.inherited('postCreate', arguments);
      this.registerDialog('create', CreateTerminologyDialog);
    },

    getEmptyListWarning() {
      return this.NLSBundle1.emptyListWarning;
    },

    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem('skosmos:conceptScheme');
      }
      return this.template;
    },
    onLimit() {
      if (!defaults.get('hasAdminRights')
        && config.terms
        && parseInt(config.terms.schemeLimit, 10) === config.terms.schemeLimit
        && config.terms.schemeLimitDialog) {
        let exception = false;
        const premiumGroupId = config.entrystore.premiumGroupId;
        if (premiumGroupId) {
          const es = defaults.get('entrystore');
          const groups = defaults.get('userEntry').getParentGroups();
          exception =
            groups.some(groupEntryURI => es.getEntryId(groupEntryURI) === premiumGroupId);
        }

        if (!exception &&
          this.getView().getSize() >= parseInt(config.terms.schemeLimit, 10)) {
          defaults.get('dialogs').restriction(config.terms.schemeLimitDialog);
          return true;
        }
      }
      return false;
    },
  });
});
