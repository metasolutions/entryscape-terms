define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dijit/_WidgetsInTemplateMixin',
  '../defaults',
  'rdforms/view/renderingContext',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/text!./CreateTerminologyTemplate.html',
  'entryscape-commons/list/common/ListDialogMixin',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'dojo/on',
  'dojo/_base/lang',
  'dojo/dom-style',
  'i18n!nls/esteScheme',
], (declare, domAttr, _WidgetsInTemplateMixin, defaults, renderingContext,
    TitleDialog, template, ListDialogMixin, _WidgetBase,
    _TemplatedMixin, NLSMixin, on, lang, domStyle) =>

  declare([_WidgetBase, _TemplatedMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    nlsBundles: ['esteScheme'],
    // * to be removed in nls * nlsHeaderTitle: 'createSchemeHeader',
    // * to be removed in nls * nlsFooterButtonLabel: 'createSchemeButton',

    postCreate() {
      this.inherited(arguments);
      on(this.schemeName, 'keyup', lang.hitch(this, this.checkValidInfoDelayed));
    },
    open() {
      if (this.list.onLimit()) {
        return;
      }
    },
    checkValidInfoDelayed() {
      if (this.delayedCheckTimout != null) {
        clearTimeout(this.delayedCheckTimout);
      }
      this.delayedCheckTimeout = setTimeout(lang.hitch(this, this.checkValidInfo), 300);
    },
    checkValidInfo() {
      const schemeName = domAttr.get(this.schemeName, 'value');
      if (schemeName === '') {
        this.dialog.lockFooterButton();
        return;
      }
      this.dialog.unlockFooterButton();
    },
    clear() {
      this.list.getView().clearSearch();
      domAttr.set(this.schemeName, 'value', '');
      domAttr.set(this.schemeDesc, 'value', '');
    },
    footerButtonAction() {
      let group;
      let hc;
      const name = domAttr.get(this.schemeName, 'value');
      const desc = domAttr.get(this.schemeDesc, 'value');
      const store = defaults.get('entrystore');
      if (name === '') {
        // TODO remove this nls string as it will never happen (checkValidInfo method above)
        return this.NLSBundle0.insufficientInfoToCreateScheme;
      }
      let context;
      return store.createGroupAndContext()
        .then((entry) => {
          group = entry;
          hc = entry.getResource(true).getHomeContext();
          context = store.getContextById(hc);
          return entry;
        })
        .then(() => {
          const pe = defaults.get('createEntry')(context, 'skos:ConceptScheme');
          const md = pe.getMetadata();
          const l = renderingContext.getDefaultLanguage();
          md.add(pe.getResourceURI(), 'rdf:type', 'skos:ConceptScheme');
          md.addL(pe.getResourceURI(), 'dcterms:title', name, l);
          if (desc) {
            md.addL(pe.getResourceURI(), 'dcterms:description', desc, l);
          }
          return pe.commit();
        })
        .then((skos) => {
          context.getEntry().then((ctxEntry) => {
            const hcEntryInfo = ctxEntry.getEntryInfo();
            hcEntryInfo.getGraph().add(ctxEntry.getResourceURI(),
              'rdf:type', 'esterms:TerminologyContext');
            // TODO remove when entrystore is changed so groups have read access
            // to homecontext metadata by default.
            // Start fix with missing metadata rights on context for group
            const acl = hcEntryInfo.getACL(true);
            acl.mread.push(group.getId());
            hcEntryInfo.setACL(acl);
            // End fix
            return hcEntryInfo.commit().then(() => {
              if (!defaults.get('hasAdminRights')) {
                this.list.entryList.setGroupIdForContext(context.getId(), group.getId());
              }
              const row = this.list.getView().addRowForEntry(skos);
              this.list.rowMetadataUpdated(row);
              const userEntry = defaults.get('userEntry');
              userEntry.setRefreshNeeded();
              return userEntry.refresh();
            });
          });
        });
    },
  }));
