define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/dom-class',
  'dijit/_WidgetsInTemplateMixin',
  'di18n/NLSMixin',
  '../defaults',
  'rdforms/view/renderingContext',
  'entryscape-commons/dialog/TitleDialog',
  'dojo/text!./CreateTerminologyDialogTemplate.html',
  'entryscape-commons/list/common/ListDialogMixin',
  'dojo/dom-construct',
  './ImportTerminology',
  './CreateTerminology',
  'i18n!nls/esteTerminology',
], (declare, domAttr, domStyle, domClass, _WidgetsInTemplateMixin, NLSMixin, defaults,
    renderingContext, TitleDialog, template, ListDialogMixin, domConstruct, ImportTerminology,
    CreateTerminology) =>

  declare([TitleDialog.ContentNLS, _WidgetsInTemplateMixin, ListDialogMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esteTerminology'],
    nlsHeaderTitle: 'createTerminology',
    nlsFooterButtonLabel: 'createTerminologyButton',

    postCreate() {
      this.inherited(arguments);
      domConstruct.place(this.__chooserHeader, this.dialog.headerExtensionNode);
      this.importTerminology = new ImportTerminology({
        list: this.list,
        dialog: this.dialog,
      }, domConstruct.create('div', null, this.__importNode));
      this.createTerminology = new CreateTerminology({
        list: this.list,
        dialog: this.dialog,
      }, domConstruct.create('div', null, this.__createNode));
    },
    open() {
      this.inherited(arguments);
      this.createTerminology.clear();
      this.importTerminology.init();
      this.dialog.show();
    },
    footerButtonAction() {
      const importTerminology = domClass.contains(this.__importTab, 'active');
      return importTerminology ? this.importTerminology.footerButtonClick() :
        this.createTerminology.footerButtonAction();
    },
    importT() {
      this.importTerminology.init();
      domStyle.set(this.__importNode, 'display', 'block');
      domStyle.set(this.__createNode, 'display', 'none');
      domClass.toggle(this.__importTab, 'active');
      domClass.toggle(this.__createTab, 'active');
    },
    createT() {
      this.createTerminology.clear();
      domStyle.set(this.__importNode, 'display', 'none');
      domStyle.set(this.__createNode, 'display', 'block');
      domClass.toggle(this.__importTab, 'active');
      domClass.toggle(this.__createTab, 'active');
    },
  }));
