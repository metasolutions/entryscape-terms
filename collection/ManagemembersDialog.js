define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-construct',
  'di18n/NLSMixin',
  'di18n/localize',
  'entryscape-terms/utils',
  'entryscape-commons/defaults',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/list/common/ListDialogMixin',
  'dojo/promise/all',
  './CollectionTree', // In template
  'i18n!nls/esteCollection',
], (declare, lang, array, domConstruct, NLSMixin, localize, utils, defaults, TitleDialog,
    ListDialogMixin, all, CollectionTree) =>
  declare([TitleDialog, ListDialogMixin, NLSMixin.Dijit], {
    maxWidth: 800,
    nlsBundles: ['esteCollection'],
    nlsHeaderTitle: 'manageMembersHeader',
    nlsFooterButtonLabel: 'manageMembersButton',

    postCreate() {
      this.inherited(arguments);
      this.collectionTree = new CollectionTree({}, domConstruct.create('div', null, this.containerNode));
    },

    localeChange() {
      this.updateLocaleStrings(this.NLSBundle0);
    },
    open(params) {
      this.row = params.row;
      this.collectionEntry = params.row.entry;
      this.showCollection();
      this.show();
      this.inherited(arguments);
    },

    showCollection() {
      const context = defaults.get('context');
      defaults.get('entrystoreutil').getEntryByType('skos:ConceptScheme', context)
        .then((csEntry) => {
          this.collectionTree.showEntry(csEntry, this.collectionEntry);
          this.collectionTree.model.checkChange = lang.hitch(this, this.updateIfCheckChanged);
        });
      this.lockFooterButton();
    },

    getCheckDelta() {
      const treeModel = this.collectionTree.getTreeModel();
      const cn = treeModel.checkedNodes;
      const ucn = treeModel.uncheckedNodes;
      let co = 0;

      co += Object.keys(cn).length;
      co += Object.keys(ucn).length;

      return co;
    },
    updateIfCheckChanged() {
      if (this.getCheckDelta() > 0) {
        this.unlockFooterButton();
      } else {
        this.lockFooterButton();
      }
    },
    conditionalHide() {
      const co = this.getCheckDelta();
      if (co === 0) {
        this.hide();
      } else {
        const b = this.NLSBundle0;
        defaults.get('dialogs').confirm(
          localize(b, 'manageMemberAbortOrNot', co), b.discardMemberChanges, b.continueMemberChanges)
          .then((discard) => {
            if (discard) { this.hide(); }
          });
      }
    },
    footerButtonAction() {
      const treeModel = this.collectionTree.getTreeModel();
      const checkedEntries = [];
      const uncheckedEntries = [];
      const checkedNodes = treeModel.checkedNodes;
      const uncheckedNodes = treeModel.uncheckedNodes;
      const collectionEntry = this.collectionEntry;
      const collectionURI = collectionEntry.getResourceURI();
      const mpromises = [];

      Object.keys(checkedNodes)
        .forEach(checkedNode => mpromises.push(treeModel.getEntry(checkedNode)
          .then(checkedEntry => checkedEntries.push(checkedEntry))));

      Object.keys(uncheckedNodes)
        .forEach(uncheckedNode => mpromises.push(treeModel.getEntry(uncheckedNode)
          .then(uncheckedEntry => uncheckedEntries.push(uncheckedEntry))));

      const self = this;
      all(mpromises).then(() => utils.isUnModified(
        checkedEntries.concat(uncheckedEntries, collectionEntry))
        .then(() => {
          const promises = [];
          array.forEach(checkedEntries, (checkedEntry) => {
            const conceptURI = checkedEntry.getResourceURI();
            collectionEntry.getMetadata().add(collectionURI, 'skos:member', conceptURI);
            checkedEntry.getMetadata().add(conceptURI, 'dcterms:partOf', collectionURI);
            promises.push(checkedEntry.commitMetadata());
          });

          array.forEach(uncheckedEntries, (uncheckedEntry) => {
            const conceptURI = uncheckedEntry.getResourceURI();
            collectionEntry.getMetadata().findAndRemove(collectionURI, 'skos:member', conceptURI);
            uncheckedEntry.getMetadata().findAndRemove(conceptURI, 'dcterms:partOf', collectionURI);
            promises.push(uncheckedEntry.commitMetadata());
          });
          if (promises.length > 0) {
            return all(promises).then(() => collectionEntry.commitMetadata().then(() => {
              self.row.render();
            }));
          }
          return all();
        }, () => {
          const b = self.NLSBundle0;
          return defaults.get('dialogs').acknowledge(b.concurrentConflictMessage,
            b.concurrentConflictOk).then(() => {
              self.showCollection();
              throw Error(b.concurrentConflictResolved);
            });
        }));
    },
  }));
