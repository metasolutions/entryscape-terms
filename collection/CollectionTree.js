define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  './CollectionTreeModel',
  '../defaults',
  'jquery',
  'entryscape-commons/tree/Tree',
  'jstree.checkbox',
], (declare, domConstruct, CollectionTreeModel, defaults, jquery, Tree) => {
  const ns = defaults.get('namespaces');
  return declare([Tree], {

    showEntry(entry, collectionEntry) {
            // this.inherited(arguments);
      if (this.model) {
        jquery(this.treeNode).jstree('destroy');
        this.model.destroy();
      }
      this.treeNode = domConstruct.create('div', null, this.domNode);
      this.model = new CollectionTreeModel({
        membershipToRootProperty: ns.expand('skos:inScheme'),
        fromRootProperty: ns.expand('skos:hasTopConcept'),
        toRootProperty: ns.expand('skos:topConceptOf'),
        toChildProperty: ns.expand('skos:narrower'),
        toParentProperty: ns.expand('skos:broader'),
        rootEntry: entry,
        domNode: this.treeNode,
        collectionEntry,
      });
    },
  });
});
