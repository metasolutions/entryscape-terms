define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  '../defaults',
  'jquery',
  'entryscape-commons/tree/TreeModel',
  'jstree.checkbox',
], (declare, lang, defaults, jquery, TreeModel) => declare([TreeModel], {
  checkedNodes: null,
  uncheckedNodes: null,

  constructor(params) {
    this.collectionEntry = params.collectionEntry;
    this.checkedNodes = {};
    this.uncheckedNodes = {};
    this.inherited(arguments);
    const jsTreeConf = {
      core: {
        multiple: true,
        check_callback: true,
        themes: {
          name: 'proton',
          responsive: true,
        },
      },
      plugins: ['checkbox'],
      checkbox: {
        three_state: false,
        tie_selection: false,
        keep_selected_style: false,
      },
    };
    this.initJsTreeConf(jsTreeConf, params.domNode);
    jquery(params.domNode).on('uncheck_node.jstree', lang.hitch(this, this.uncheckedNode));
    jquery(params.domNode).on('check_node.jstree', lang.hitch(this, this.checkedNode));
  },
  uncheckedNode(e, data) {
    data.event.stopPropagation();
    const uri = this.getResourceURIFromNode(data.node);
    if (this.checkedNodes[uri]) {
      delete this.checkedNodes[uri];
    } else {
      this.uncheckedNodes[uri] = data.node;
    }
    this.checkChange();
  },
  checkedNode(e, data) {
    data.event.stopPropagation();
    const uri = this.getResourceURIFromNode(data.node);
    if (this.uncheckedNodes[uri]) {
      delete this.uncheckedNodes[uri];
    } else {
      this.checkedNodes[uri] = data.node;
    }
    this.checkChange();
  },
  checkChange() {
  },
  createNode(entry) {
    this.inherited(arguments);
    const uri = entry.getResourceURI();
    const node = this.uri2node[uri];
    let isChecked = false;
    if (this.collectionEntry) {
      const collection = entry.getMetadata().find(entry.getResourceURI(), 'dcterms:partOf', this.collectionEntry.getResourceURI());
      isChecked = collection.length !== 0;
    }
    node.state = { checked: isChecked };// get from entry metadata -partof
    return node;
  },
  getResourceURIFromNode(node) {
    const id = typeof node === 'object' ? node.id : node;
    let uri;
    if (id === '#') {
      uri = this.rootEntry.getResourceURI();
    } else {
      uri = id;
    }
    return uri;
  },
}));
