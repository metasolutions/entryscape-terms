define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  '../defaults',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/list/common/ETBaseList',
  'entryscape-commons/list/common/RemoveDialog',
  'entryscape-commons/rdforms/RDFormsEditDialog',
  'dojo/_base/array',
  'di18n/localize',
  './ManagemembersDialog',
  'dojo/promise/all',
  'entryscape-commons/export/Export',
  'i18n!nls/escoList',
  'i18n!nls/esteCollectionexport',
  'i18n!nls/esteCollection',
], (declare, domAttr, defaults, EntryRow, ETBaseList, RemoveDialog, RDFormsEditDialog,
    array, localize, ManagemembersDialog, all, Export) => {
  const ns = defaults.get('namespaces');

  const CDialog = declare(RDFormsEditDialog, {
    explicitNLS: true,
    maxWidth: 800,
    constructor(params) {
      this.list = params.list;
    },
    open() {
      this.list.getView().clearSearch();
      this.doneLabel = this.list.nlsSpecificBundle.createCollectionButton;
      this.title = this.list.nlsSpecificBundle.createCollectionHeader;
      this.updateTitleAndButton();
      const nds = defaults.get('createEntry')(null, 'skos:Collection');
      this._newCollection = nds;
      nds.getMetadata().add(nds.getResourceURI(), ns.expand('rdf:type'), ns.expand('skos:Collection'));
      this.show(
        nds.getResourceURI(), nds.getMetadata(),
        this.list.getTemplate(), this.list.getTemplateLevel());
    },
    doneAction(graph) {
      this._newCollection.setMetadata(graph).commit().then((newEntry) => {
        this.list.getView().addRowForEntry(newEntry);
        return newEntry.refresh();
      });
    },
  });
  const CRemoveDialog = declare(RemoveDialog, {
    open(params) {
      this.collectionEntry = params.row.entry;
      this.inherited(arguments);
    },
    remove() {
      return this.removeHasPartRelations().then(() => this.collectionEntry.del());
    },
    removeHasPartRelations() {
      const members = [];
      const promises = [];
      const curi = this.collectionEntry.getResourceURI();
      return defaults.get('entrystore')
        .newSolrQuery().rdfType('skos:Concept').uriProperty('dcterms:partOf', curi)
        .list()
        .forEach((m) => {
          members.push(m);
        })
        .then(() => {
          array.forEach(members, (m) => {
            m.getMetadata().findAndRemove(m.getResourceURI(), 'dcterms:partOf', curi);
            promises.push(m.commitMetadata());
          });
          return all(promises);
        });
    },
  });

  const CollectionRow = declare([EntryRow], {
    showCol1: true,
    renderCol1() {
      const count = this.entry.getMetadata().find(this.entry.getResourceURI(), 'skos:member').length;
      console.log(this);
      $(this.domNode).addClass('termsCollection');
      domAttr.set(this.col1Node, 'innerHTML', `<span class="badge">${count}</span>`);
      if (this.nlsSpecificBundle) {
        domAttr.set(this.col1Node, 'title',
          localize(this.nlsSpecificBundle, 'collectionMembers', count));
      }
    },
    updateLocaleStrings() {
      this.inherited(arguments);
      this.renderCol1();
    },
  });

  const ExportDialog = declare([Export], {
    nlsBundles: ['esteCollectionexport'],
    nlsHeaderTitle: 'exportHeaderLabel',
    title: 'temporary', // to avoid exception
    profile: 'skoscollection',
    open(params) {
      const name = defaults.get('rdfutils').getLabel(params.row.entry);
      this.title = localize(this.NLSBundle0, 'exportHeaderLabel', { name });
      this.localeChange();
      this.inherited(arguments);
    },
  });

  return declare([ETBaseList], {
    includeCreateButton: true,
    includeInfoButton: false,
    includeEditButton: true,
    includeRemoveButton: true,
    nlsBundles: ['escoList', 'esteCollection'],
    entryType: ns.expand('skos:Collection'), // change
    entitytype: 'conceptcollection',
    nlsEditEntryTitle: 'editCollectionTitle',
    nlsEditEntryLabel: 'editCollection',
    nlsRemoveEntryLabel: 'removeCollection',
    nlsRemoveEntryTitle: 'removeCollectionTitle',
    nlsCreateEntryLabel: 'createCollection',
    nlsCreateEntryTitle: 'createCollectionPopoverTitle',
    searchVisibleFromStart: false,
    rowClass: CollectionRow,
    rowClickDialog: 'members',
    versionExcludeProperties: ['skos:member'],
    rowActionNames: ['edit', 'versions', 'members', 'export', 'remove'],

    postCreate() {
      this.registerDialog('members', ManagemembersDialog);
      this.registerRowAction({
        name: 'members',
        button: 'default',
        icon: 'check-square-o',
        iconType: 'fa',
        nlsKey: 'manageMembers',
        nlsKeyTitle: 'manageMembersTitle',
      });
      this.registerDialog('export', ExportDialog);
      this.registerRowAction({
        first: true,
        name: 'export',
        button: 'default',
        icon: 'arrow-circle-o-down',
        iconType: 'fa',
        nlsKey: 'collectionExportTitle',
      });
      this.inherited('postCreate', arguments);
      this.registerDialog('create', CDialog);
      this.registerDialog('remove', CRemoveDialog);
      this.dialogs.create.levels.setIncludeLevel('optional');
    },
    show() {
      const self = this;
      const esu = defaults.get('entrystoreutil');
      esu.preloadEntries(ns.expand('skos:Collection'), null).then(self.render());
    },
    getSearchObject() {
      const context = defaults.get('context');
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().rdfType('skos:Collection').context(context);
    },
    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem('skosmos:conceptScheme');
      }
      return this.template;
    },
    getTemplateLevel() {
      return 'recommended';
    },
  });
});
